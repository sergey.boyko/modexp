//
// Created by bso on 11.04.19.
//

#ifndef MODEXP_DETAIL_MODEXP_DEFINES_H
#define MODEXP_DETAIL_MODEXP_DEFINES_H

namespace modexp::detail {

enum {
  default_value_k = 0
};

namespace base_interval {
enum {
  lower_bound_k = 0,
  upper_bound_k = 1'000'000'000,
};
} // end of base_interval

namespace exponent_interval {
enum {
  lower_bound_k = 0,
  upper_bound_k = 1'000'000,
};
} // end of exponent_interval

namespace mod_interval {
enum {
  lower_bound_k = 0,
  upper_bound_k = 1'000'000'000,
};
} // end of mod_interval

} // end of modexpr::detail

#endif //MODEXP_DETAIL_MODEXP_DEFINES_H
