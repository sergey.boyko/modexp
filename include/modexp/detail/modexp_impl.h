//
// Created by bso on 11.04.19.
//

#ifndef MODEXP_DETAIL_MODEXP_IMPL_H
#define MODEXP_DETAIL_MODEXP_IMPL_H

#include <cstdint>

#include "modexp/detail/modexp_defines.h"

namespace modexp::detail {

/**
 * Calculate the following expression:
 *   (x * y) % mod
 *
 * x and y arguments is uint64_t,
 * because the x * y operation can take the value that doesn't fit to uint32_t
 * but after '% mod' operation the result will be <= mod (uint32_t)
 * @param x - unsigned long
 * @param y - unsigned long long
 * @param mod - unsigned integer
 * @return - calculation result (unsigned integer)
 */
constexpr std::uint32_t ModByMultiplication(std::uint64_t x, std::uint64_t y, std::uint32_t mod) {
  return static_cast<std::uint32_t>((x * y) % mod);
}

/**
 * Modular exponentiation based on recursion and the following idea:
 *
 * (x * y) % mod === (x % mod) * (y % mod) % mod
 *
 * @param base - unsigned integer
 * @param exponent - unsigned integer
 * @param mod - unsigned integer
 * @return - calculation result
 */
constexpr std::uint32_t ModularExponentiationImpl(std::uint32_t base,
                                                  std::uint32_t exponent,
                                                  std::uint32_t mod) {
  // if the base == 0, then in any subsequent step the result will be 0,
  // because multiplication by 0 is always is 0
  if (!base) {
    return 0;
  }

  // if the exponent == 0, then in any subsequent step the result will be 1
  // because any number to the 0 power always gives a 1
  if (!exponent) {
    return 1;
  }

  auto next_step_exponent = exponent >> 1;

  // call itself with the next_step_exponent
  auto result = ModularExponentiationImpl(base, next_step_exponent, mod);

  // put the '(result ^ 2) % mod' to the result
  result = ModByMultiplication(result, result, mod);

  auto is_exponent_even = static_cast<bool>(exponent % 2);
  if (is_exponent_even) {
    // return '(result * base) % mod'
    return ModByMultiplication(result, base, mod);
  }

  // exponent is odd
  return result;
}

/**
* Check if the function arguments match declared intervals
* @param base
* @param exponent
* @param mod
* @return
*/
constexpr bool CheckIntervals(std::uint32_t base,
                              std::uint32_t exponent,
                              std::uint32_t mod) {
  auto is_match_interval
      = [](std::uint32_t val,
           std::uint32_t lower_bound,
           std::uint32_t upper_bound) {
        return val > lower_bound && val < upper_bound;
      };

  return is_match_interval(base, base_interval::lower_bound_k, base_interval::upper_bound_k)
         && is_match_interval(exponent, exponent_interval::lower_bound_k, exponent_interval::upper_bound_k)
         && is_match_interval(mod, mod_interval::lower_bound_k, mod_interval::upper_bound_k);
}

} // end of modexp::detail

#endif //MODEXP_DETAIL_MODEXP_IMPL_H
