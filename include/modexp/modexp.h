//
// Created by bso on 11.04.19.
//

#ifndef MODEXP_MODEXP_H
#define MODEXP_MODEXP_H

#include <utility>
#include "modexp/detail/modexp_impl.h"

namespace modexp {

/**
* Enumeration of the result values
*/
enum class result {
  ok_k = 0,
  error_k
};

/**
* Modular exponentiation
* (base ^ exponent) % mod
*
* The function can be used only at compile time
* @param base - the unsigned integer
* @param exponent - the unsigned integer
* @param mod - the unsigned integer
* @return - calculation result
*/
template <std::uint32_t base, std::uint32_t exponent, std::uint32_t mod>
constexpr std::uint32_t ModularExponentiation() {
  static_assert(detail::CheckIntervals(base, exponent, mod));
  return detail::ModularExponentiationImpl(base, exponent, mod);
}

/**
* Modular exponentiation
* (base ^ exponent) % mod
*
* The function can be used both at the compile time and at the runtime.
* Because of this universality the input arguments static validation doesn't seem possible
* @param base - the unsigned integer
* @param exponent - the unsigned integer
* @param mod - the unsigned integer
* @return - first: success indicator
*           second: calculation result
*/
constexpr std::pair<result, std::uint32_t> ModularExponentiation(std::uint32_t base,
                                                                 std::uint32_t exponent,
                                                                 std::uint32_t mod) {
  if (!detail::CheckIntervals(base, exponent, mod)) {
    // the function can be called for non-constexpr arguments
    // because of this the static_assert cannot be used here
    return {result::error_k, detail::default_value_k};
  }

  return {result::ok_k, detail::ModularExponentiationImpl(base, exponent, mod)};
}

}

#endif //MODEXP_MODEXP_H
