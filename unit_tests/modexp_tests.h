//
// Created by bso on 11.04.19.
//

#include <gtest/gtest.h>
#include <iostream>

#include "modexp/modexp.h"

TEST(ModexpTest, IsResultConstexpr) {
  // success if the test is compiled
  static_assert(modexp::ModularExponentiation(131'999'920, 234'979, 991'540'365).first
                == modexp::result::ok_k);
  static_assert(modexp::ModularExponentiation<131'999'920, 234'979, 991'540'365>() > 0);
}

TEST(ModexpTest, IsRuntimeable) {
  // success if the test is compiled
  const auto base = 131'999'920;
  const auto exponent = 234'979;
  const auto mod = 991'540'365;
  ASSERT_TRUE(modexp::ModularExponentiation(base, exponent, mod).first
              == modexp::result::ok_k);
}

TEST(ModexpTest, IncorrectInputArguments) {
  ASSERT_TRUE(
      modexp::ModularExponentiation(
          modexp::detail::base_interval::lower_bound_k,
          124,
          6'262).first
      == modexp::result::error_k);
  ASSERT_TRUE(
      modexp::ModularExponentiation(
          3,
          modexp::detail::exponent_interval::lower_bound_k,
          6'262).first
      == modexp::result::error_k);
  ASSERT_TRUE(
      modexp::ModularExponentiation(
          3,
          1'251,
          modexp::detail::mod_interval::lower_bound_k).first
      == modexp::result::error_k);
  ASSERT_TRUE(
      modexp::ModularExponentiation(
          modexp::detail::base_interval::upper_bound_k,
          124,
          6'262).first
      == modexp::result::error_k);
  ASSERT_TRUE(
      modexp::ModularExponentiation(
          3,
          modexp::detail::exponent_interval::upper_bound_k,
          6'262).first
      == modexp::result::error_k);
  ASSERT_TRUE(
      modexp::ModularExponentiation(
          3,
          1'251,
          modexp::detail::mod_interval::upper_bound_k).first
      == modexp::result::error_k);

  // compile error
  // modexp::ModularExponentiation<
  //     modexp::detail::base_interval::lower_bound_k,
  //     124,
  //     6262>();
}

TEST(ModexpTest, IsWorkCorrectly) {
  // success if the test is compiled
  constexpr auto res11
      = modexp::ModularExponentiation(131'999'920, 234'979, 991'540'365);
  // test the ModularExponentiation based on template parameters
  // just one time, because the implementation of the calculation
  // is the same as ModularExponentiation based on arguments
  constexpr auto res12
      = modexp::ModularExponentiation<131'999'920, 234'979, 991'540'365>();

  constexpr auto res2
      = modexp::ModularExponentiation(131'999'920, 234'972, 2);

  constexpr auto res3
      = modexp::ModularExponentiation(252'965'189, 234'972, 412'424'124);

  constexpr auto res4
      = modexp::ModularExponentiation(14, 3, 3);

  ASSERT_TRUE(res11.first == modexp::result::ok_k
              && res11.second == 682771105);
  ASSERT_TRUE(res12 == 682771105);

  ASSERT_TRUE(res2.first == modexp::result::ok_k
              && res2.second == 0);

  ASSERT_TRUE(res3.first == modexp::result::ok_k
              && res3.second == 258'288'997);

  ASSERT_TRUE(res4.first == modexp::result::ok_k
              && res4.second == 2);
}
