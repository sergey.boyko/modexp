/**
* Created by Sergey O. Boyko on 11.04.19.
*/

#include <gtest/gtest.h>
#include "modexp_tests.h"

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
